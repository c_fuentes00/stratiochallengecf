package cl.cf.stratio.challenge.mafia

import scala.collection.mutable.ArrayBuffer


class MafiaOrg 
  (
      var name: String, //Name of the organization
      var imprisonedMembers: ArrayBuffer[MafiaMember], //List of imprisoned members
      var bigBoss: MafiaMember //Main boss of the organization
  )
{
  
  //Add member to list of imprisoned members, create list if doesn't exists
  def addImprisonedMember(member: MafiaMember){
    if ( imprisonedMembers == null ){
      this.imprisonedMembers = ArrayBuffer()
    }
    this.imprisonedMembers += member
  }
  
  //Remove member from list of imprisoned members
  def removeImprisonedMember(member: MafiaMember){
    if ( imprisonedMembers == null ){
      this.imprisonedMembers = ArrayBuffer()
    }
    this.imprisonedMembers -= member
  }
}