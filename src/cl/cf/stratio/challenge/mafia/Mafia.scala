package cl.cf.stratio.challenge.mafia

import scala.collection.mutable.ArrayBuffer

//Main Mafia class (to define operations)
//1.- When a member goes to jail (imprisoned)
//2.- When a member is release fro jail
//3.- To found out when a boss has more than determinate number of subordinates 

class Mafia 
  (
      var organizations: ArrayBuffer[MafiaOrg] 
  )
{
  //***MAIN OPERATIONS**/
  
  //1.- Go to Jail
  /*When an organization member goes to jail, he temporarily disappears from the organization. All
  his direct subordinates are immediately relocated and now work for the oldest remaining boss at
  the same level as their previous boss. If there is no such possible alternative boss the oldest direct
  subordinate of the previous boss is promoted to be the boss of the others.*/
  
  def goToJail(orgName: String, imprisonedMember :MafiaMember ){
    for ( org <- this.organizations ){
      
      if ( org.name == orgName ){
        org.addImprisonedMember(imprisonedMember);
        
        //Remove from list of subordinates
        var imprisonedBoss = imprisonedMember.boss 
        
        if ( imprisonedBoss != null ){     
          var sameLevelSub = imprisonedBoss.subordinates
          sortMember(sameLevelSub)
          imprisonedBoss.removeSuborditante( imprisonedMember )
        }
        
        //Relocate subordinates to the oldest remaining boss
        var imprisonedSub = imprisonedMember.subordinates
		    if ( imprisonedSub != null){
		      
		      //Find and assing a new boss at the same level
		      if ( imprisonedBoss.subordinates != null  && !imprisonedBoss.subordinates.isEmpty){
    		        for ( subordinate <- imprisonedSub  ){
    		          imprisonedBoss.subordinates(0).addSubordinate(subordinate)
    		          subordinate.boss = imprisonedBoss.subordinates(0)
    		        }
		      }
		      else
		      {
		        
    		        //Assign oldest direct subordinate as boss
    		        if ( imprisonedSub.size > 0 ){
    		          var oldest = imprisonedSub(0);
    		          imprisonedMember.removeSuborditante(oldest)
    		          
    		          for (subordinate <- imprisonedSub){
    		            oldest.addSubordinate(subordinate)
    		            subordinate.boss=oldest
    		          }

             		imprisonedBoss.addSubordinate(oldest);
            			oldest.boss = imprisonedBoss;   		          
    		           imprisonedMember.newSubordinateBoss=oldest
    		        }
		      }
		    }  
      } 
    }
  }
  
  //2.- Release From Prison
  /*
  When the imprisoned member is released from prison, he immediately recovers his old position
	in the organization (meaning that he will have the same boss that he had at the moment of being
	imprisoned). All his former direct subordinates are transferred to work for the recently released
	member, even if they were previously promoted or have a different boss now.
	* 
	*/
  def releaseFromPrison(orgName: String, releasedMember :MafiaMember ){
    
    for ( org <- this.organizations ){
      
      if ( org.name == orgName ){
        
        //transferring former subordinates to work for released member       
        var releaseSubordinates = releasedMember.subordinates

        if ( releaseSubordinates != null ) {        
          for(subordinate<- releaseSubordinates){
      				var oldBoss = subordinate.boss;
      				oldBoss.removeSuborditante(subordinate);
      				subordinate.boss = releasedMember
			    }
        }
        
        //Back to subordinate list of release boss
        if ( releasedMember.newSubordinateBoss != null ){
          releasedMember.addSubordinate(releasedMember.newSubordinateBoss)
          releasedMember.boss.removeSuborditante(releasedMember.newSubordinateBoss)
        }
        
        releasedMember.boss.addSubordinate(releasedMember)          
        org.removeImprisonedMember(releasedMember);
      }
    }
  }
  
  //method to find out whether a boss has more than 50 people under him.
  def exceedSubordinates( boss: MafiaMember, bossInSurveillance: String, maxSubordinates: Int ) :Boolean = {
    
//        require(boss.name != bossInSurveillance)
        var ret = false
        if ( boss.name == bossInSurveillance && boss.subordinates != null){
          return boss.subordinates.size > maxSubordinates
        }
        else{
          if ( boss.subordinates != null
              && boss.subordinates.size > 0 ){
            for ( member <- boss.subordinates ){
             ret = exceedSubordinates(member, bossInSurveillance, maxSubordinates)
             if ( ret ) return ret
            }
          }
        }
        
        return false
  }
  
  //****OTHER OPERATIONS****/
  
   //sort members Array
  def sortMember(members :ArrayBuffer[MafiaMember]){
    members.sorted
  } 
  
  //add organization, create array if doesn't exists 
  def addOrganization(organization: MafiaOrg){
    if ( organizations == null ){
      this.organizations = ArrayBuffer()
    }
    this.organizations += organization
  }  
  
}