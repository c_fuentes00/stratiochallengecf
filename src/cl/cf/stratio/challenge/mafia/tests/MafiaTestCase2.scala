package cl.cf.stratio.challenge.mafia.tests

import cl.cf.stratio.challenge.mafia.MafiaOrg
import cl.cf.stratio.challenge.mafia.MafiaMember
import cl.cf.stratio.challenge.mafia.Mafia
import java.util.Date

object MafiaTestCase2 {
  
  def main(args: Array[String]){
      
       //Create objects Case 2 TEST 
       var cbigBoss = new MafiaMember("Big Boss 2", new Date(), null,null)
       var cmafiaOrg = new MafiaOrg("MafiaOrgTest2", null, cbigBoss)
       var cboss1 = new MafiaMember("Boss 1", new Date(), cbigBoss,null)
        var csubordinate1_1 = new MafiaMember("Sub-Boss 1.1", new Date(), cboss1,null)
        cboss1.addSubordinate(csubordinate1_1)
        
        var csubBoss1_2 = new MafiaMember("Sub-Boss 1.2", new Date(), cboss1,null)
        cboss1.addSubordinate(csubBoss1_2)
        
        var csubordinate1_2_1 = new MafiaMember("Sub-Boss 1.2.1", new Date(), csubBoss1_2,null)
        csubBoss1_2.addSubordinate(csubordinate1_2_1)
        
        var csubordinate1_2_2 = new MafiaMember("Sub-Boss 1.2.2", new Date(), csubBoss1_2,null)
        csubBoss1_2.addSubordinate(csubordinate1_2_2)
        
        var csubordinate1_2_3 = new MafiaMember("Sub-Boss 1.2.3", new Date(), csubBoss1_2,null)
        csubBoss1_2.addSubordinate(csubordinate1_2_3)
        
        var csubordinate1_2_4 = new MafiaMember("Sub-Boss 1.2.4", new Date(), csubBoss1_2,null)
        csubBoss1_2.addSubordinate(csubordinate1_2_4)
  
        var csubBoss1_3 = new MafiaMember("Sub-Boss 1.3", new Date(), cboss1,null)
        cboss1.addSubordinate(csubBoss1_3)    
        
        cbigBoss.addSubordinate(cboss1)
        
        var cmafia = new Mafia(null)
        cmafia.addOrganization(cmafiaOrg)
        
      println("=========================")
      println("***********CASE 2********")
      println("=========================")
      println("--------INTIAL STATE--------")
      printResult(cmafia)      
      println("--------GO TO JAIL----------")
      
      cmafia.goToJail("MafiaOrgTest2", cboss1)
      printResult(cmafia)
      
      if ( cmafia.organizations(0).imprisonedMembers != null ){
        for (imprisonedMember <- cmafia.organizations(0).imprisonedMembers ){
          println("Imprisoned Member Name --> " + imprisonedMember.name)
          
          //Release imprisoned member
          println("--------RELEASE FROM JAIL----------")      
          cmafia.releaseFromPrison("MafiaOrgTest2", imprisonedMember)
          printResult(cmafia)
        }
      }        
      
      println(" ")
      println("--------FIND OUT NUMBERS OF SUBORDINATES----------")
      var hasMoreThat = cmafia.exceedSubordinates(cbigBoss, "Boss 1", 2) //Number for execute the test
      println( "Boss 1 HAS MORE THAN <3> SUBORDINATES : " + hasMoreThat )
       
    }
    
    //Print result Case2
    def printResult(mafia: Mafia){
      println("BIG BOSS: " + mafia.organizations(0).bigBoss.name)      
      for (member <- mafia.organizations(0).bigBoss.subordinates ){
        println("->Member Name: " + member.name)
        if ( member.subordinates != null ){
          for (subBoss <- member.subordinates){
            println("--->Sub Boss Name: " + subBoss.name)
            if ( subBoss.subordinates != null ){
              for (sub <- subBoss.subordinates){
                println("----->Subordinate: " + sub.name)
              }
            }
          }
        }
      }      
    }
}