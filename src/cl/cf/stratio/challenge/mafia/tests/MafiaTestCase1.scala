package cl.cf.stratio.challenge.mafia.tests

import cl.cf.stratio.challenge.mafia.MafiaOrg
import cl.cf.stratio.challenge.mafia.MafiaMember
import cl.cf.stratio.challenge.mafia.Mafia
import java.util.Date

object MafiaTestCase1 {
  
  def main(args: Array[String]){
      
      //Create objects Case 1 TEST  
      var bigBoss = new MafiaMember("Big Boss", new Date(), null,null)
      var mafiaOrg = new MafiaOrg("MafiaOrgTest", null, bigBoss)
      
      
      var boss1 = new MafiaMember("Boss 1", new Date(), bigBoss,null)
      var subordinate1_1 = new MafiaMember("Sub-Boss 1.1", new Date(), boss1,null)
      boss1.addSubordinate(subordinate1_1)
      
      var subBoss1_2 = new MafiaMember("Sub-Boss 1.2", new Date(), boss1,null)
      boss1.addSubordinate(subBoss1_2)
      
      var subordinate1_2_1 = new MafiaMember("Sub-Boss 1.2.1", new Date(), subBoss1_2,null)
      subBoss1_2.addSubordinate(subordinate1_2_1)
      
      var subordinate1_2_2 = new MafiaMember("Sub-Boss 1.2.2", new Date(), subBoss1_2,null)
      subBoss1_2.addSubordinate(subordinate1_2_2)
      
      var subordinate1_2_3 = new MafiaMember("Sub-Boss 1.2.3", new Date(), subBoss1_2,null)
      subBoss1_2.addSubordinate(subordinate1_2_3)
      
      var subordinate1_2_4 = new MafiaMember("Sub-Boss 1.2.4", new Date(), subBoss1_2,null)
      subBoss1_2.addSubordinate(subordinate1_2_4)

      var subBoss1_3 = new MafiaMember("Sub-Boss 1.3", new Date(), boss1,null)
      boss1.addSubordinate(subBoss1_3)
      
      
      var boss2 = new MafiaMember("Boss 2", new Date(), bigBoss,null)
      var subordinate2_1 = new MafiaMember("subordinate 2.1", new Date(), boss2,null)
      boss2.addSubordinate(subordinate2_1)
      var subordinate2_2 = new MafiaMember("subordinate 2.2", new Date(), boss2,null)
      boss2.addSubordinate(subordinate2_2)
      var subordinate2_3 = new MafiaMember("subordinate 2.3", new Date(), boss2,null)
      boss2.addSubordinate(subordinate2_3)
      var subordinate2_4 = new MafiaMember("subordinate 2.4", new Date(), boss2,null)
      boss2.addSubordinate(subordinate2_4)
      
      var boss3 = new MafiaMember("Boss 3", new Date(), bigBoss,null)
      
      bigBoss.addSubordinate(boss1)
      bigBoss.addSubordinate(boss2)
      bigBoss.addSubordinate(boss3)
      
      //create mafia and assign organization      
      var mafia = new Mafia(null)
      mafia.addOrganization(mafiaOrg)
            
      println("=========================")
      println("***********CASE 1********")
      println("=========================")
      println("--------INTIAL STATE--------")
      printResult(mafia)      
      println("--------GO TO JAIL----------")      
      mafia.goToJail("MafiaOrgTest", boss1)
      printResult(mafia)
      
      if ( mafia.organizations(0).imprisonedMembers != null ){
        for (imprisonedMember <- mafia.organizations(0).imprisonedMembers ){
          println("Imprisoned Member Name --> " + imprisonedMember.name)
          
          //Release imprisoned member
          println("--------RELEASE FROM JAIL----------")      
          mafia.releaseFromPrison("MafiaOrgTest", imprisonedMember)
          printResult(mafia)
          
        }
      }

      println(" ")
      println("--------FIND OUT NUMBERS OF SUBORDINATES----------")
      val hasMoreThat = mafia.exceedSubordinates(bigBoss, "Sub-Boss 1.2", 3)
      println( "Sub-Boss 1.2 HAS MORE THAN <2> SUBORDINATES : " + hasMoreThat )
      
    }
    
    //Print result Case2
     def printResult(mafia: Mafia){
      println("BIG BOSS: " + mafia.organizations(0).bigBoss.name)      
      for (member <- mafia.organizations(0).bigBoss.subordinates ){
        println("->Member Name: " + member.name)
        if ( member.subordinates != null ){
          for (subBoss <- member.subordinates){
            println("--->Sub Boss Name: " + subBoss.name)
            if ( subBoss.subordinates != null ){
              for (sub <- subBoss.subordinates){
                println("----->Subordinate: " + sub.name)
              }
            }
          }
        }
      }      
    }   
}