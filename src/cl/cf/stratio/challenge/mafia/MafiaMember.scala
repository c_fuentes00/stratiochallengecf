package cl.cf.stratio.challenge.mafia

import java.util.Date
import scala.collection.mutable.ArrayBuffer

class MafiaMember 
  (
    var name: String, //Mafia member Name
    var initialDate: Date, //Initial date in the organization
    var boss: MafiaMember, //Boss of the subordinate
    var subordinates: ArrayBuffer[MafiaMember] //Members of the group (subordinates)
  )
  extends Ordered[MafiaMember]{
  
  var newSubordinateBoss : MafiaMember = null
  
  //Add to "subordinates" list, create list if it doesn't exists
  def addSubordinate(subordinate: MafiaMember){
    if ( subordinates == null ){
      this.subordinates = ArrayBuffer()
    }
    this.subordinates += subordinate
  }
  
  //Remove from "subordinates" list
  def removeSuborditante(subordinate: MafiaMember){
    if ( subordinates != null ){
       this.subordinates -= subordinate
    }
  }
  
  //Override compare method
  def compare(that: MafiaMember) = this.initialDate.compareTo(that.initialDate)
  
}